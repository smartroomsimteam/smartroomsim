package srsim.test.simulator;

import java.sql.SQLException;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import srsim.persistence.Criterion;
import srsim.persistence.DerbyEmbeddedDS;
import srsim.persistence.SimulationRecord;

public class PersistenceTest {

	@Test
	public void testDerbyConnection() throws InstantiationException,
			IllegalAccessException, ClassNotFoundException, SQLException {
		new DerbyEmbeddedDS();
	}

	@Test
	public void testPersistingAndRetrievingRecord()
			throws InstantiationException, IllegalAccessException,
			ClassNotFoundException, SQLException {

		DerbyEmbeddedDS ds = new DerbyEmbeddedDS();

		SimulationRecord record = new SimulationRecord();
		record.setBrightness(100.0D);
		record.setTemperature(15.0D);
		record.setEnergyConsumption(1400.0D);
		record.setTimeStamp(System.currentTimeMillis());
		ds.persist(record);
		Criterion criterion = new Criterion("temperature").lessEqual("20").and(
				new Criterion("brightness").lessThan("10000"));
		List<SimulationRecord> storedRecords = ds.find(criterion);
		Assert.assertFalse(storedRecords.isEmpty());
	}

	@Test
	public void testRetrievingAllRecords() throws InstantiationException,
			IllegalAccessException, ClassNotFoundException, SQLException {

		DerbyEmbeddedDS ds = new DerbyEmbeddedDS();
		List<SimulationRecord> storedRecords = ds.findAll();
		Assert.assertTrue(storedRecords.size() > 1);
	}

	@Test
	public void testDeletingRecord() throws InstantiationException,
			IllegalAccessException, ClassNotFoundException, SQLException {
		DerbyEmbeddedDS ds = new DerbyEmbeddedDS();
		SimulationRecord record = new SimulationRecord();
		record.setBrightness(42.0D);
		record.setTemperature(42.0D);
		record.setEnergyConsumption(42.0D);
		long timeStamp = System.currentTimeMillis();
		record.setTimeStamp(timeStamp);
		ds.persist(record);
		Criterion criterion = new Criterion("timestamp").equal(String.valueOf(timeStamp));
		List<SimulationRecord> storedRecords = ds.find(criterion);
		Assert.assertFalse(storedRecords.isEmpty());
		record = storedRecords.get(0);
		ds.delete(record);
		storedRecords = ds.find(criterion);
		Assert.assertTrue(storedRecords.isEmpty());
	}

}
