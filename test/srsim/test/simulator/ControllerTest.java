package srsim.test.simulator;

import org.junit.Assert;
import org.junit.Test;

import srsim.actuators.HeatingActuator;
import srsim.controller.HeatingController;
import srsim.domain.IActuator;
import srsim.domain.IController;
import srsim.domain.ISensor;
import srsim.domain.Room;
import srsim.domain.Simulation;
import srsim.sensors.TemperatureSensor;
import srsim.simulator.SimulationConfigurationException;
import srsim.simulator.SimulationContextException;
import srsim.simulator.SystemTimeTimeSource;

public class ControllerTest {

	@Test
	public void testAttachingSensors() throws SimulationConfigurationException {
		IController controller = new HeatingController();
		ISensor sensor = new TemperatureSensor();
		IActuator actuator = new HeatingActuator();
		controller.attachSensor(sensor);
		controller.attachActuator(actuator);
	}
	
	@Test
	public void testInitiatingActuatorChangeFromSensorUpdate() throws SimulationContextException {
		Simulation simulation = new Simulation(new SystemTimeTimeSource());
		Room room = new Room();
		HeatingController controller = new HeatingController();
		ISensor sensor = new TemperatureSensor();
		IActuator actuator = new HeatingActuator();
		room.setContext(simulation.getContext());
		simulation.getContext().setTemperature(0.0D);
		simulation.getContext().setPreference("targetTemperature","21.5D");
		controller.attachSensor(sensor);
		controller.attachActuator(actuator);
		Assert.assertTrue(actuator.getState() == IActuator.IDLE);
		room.addSensor(sensor);
		room.addController(controller);
		room.addActuator(actuator);
		controller.step();
		Assert.assertTrue(actuator.getState() == IActuator.ACTIVE);
	}
	

}
