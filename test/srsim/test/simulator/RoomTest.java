package srsim.test.simulator;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import srsim.actuators.HeatingActuator;
import srsim.controller.HeatingController;
import srsim.domain.IActuator;
import srsim.domain.IController;
import srsim.domain.ISensor;
import srsim.domain.Room;
import srsim.sensors.TemperatureSensor;
import srsim.simulator.SimulationContext;
import srsim.simulator.SimulationContextException;
import srsim.simulator.SystemTimeTimeSource;

public class RoomTest {

	@Test
	public void testAddingSensors() throws SimulationContextException {
		Room room = new Room();
		room.setContext(new SimulationContext(new SystemTimeTimeSource()));
		TemperatureSensor sensor = new TemperatureSensor();
		room.addSensor(sensor);
		List<ISensor> sensors = room.getSensors();
		Assert.assertSame(sensor, sensors.get(0));
	}
	
	@Test
	public void testAddingActuators() throws SimulationContextException {
		Room room = new Room();
		room.setContext(new SimulationContext(new SystemTimeTimeSource()));
		HeatingActuator actuator = new HeatingActuator();
		room.addActuator(actuator);
		List<IActuator> actuators = room.getActuators();
		Assert.assertSame(actuator, actuators.get(0));
	}
	
	@Test
	public void testAddingControllers() throws SimulationContextException {
		Room room = new Room();
		room.setContext(new SimulationContext(new SystemTimeTimeSource()));
		HeatingController controller = new HeatingController();
		room.addController(controller);
		List<IController> controllers = room.getControllers();
		Assert.assertSame(controller, controllers.get(0));
	}

}
