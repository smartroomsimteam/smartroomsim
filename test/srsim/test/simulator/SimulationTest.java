package srsim.test.simulator;

import org.junit.Assert;
import org.junit.Test;

import srsim.actuators.HeatingActuator;
import srsim.actuators.LightingActuator;
import srsim.controller.HeatingController;
import srsim.controller.LightingController;
import srsim.domain.AbstractController;
import srsim.domain.IActuator;
import srsim.domain.ISensor;
import srsim.domain.Room;
import srsim.domain.Simulation;
import srsim.sensors.LightSensor;
import srsim.sensors.TemperatureSensor;
import srsim.simulator.SimulationConfigurationException;
import srsim.simulator.SimulationContextException;
import srsim.simulator.SystemTimeTimeSource;

public class SimulationTest {

	@Test
	public void testTemperatureChangeThroughActuator()
			throws SimulationContextException, InterruptedException {
		double targetTemperature = 21.5D;
		Simulation simulation = new Simulation(new SystemTimeTimeSource());
		simulation.setResolution(0);
		Room room = new Room();
		HeatingController controller = new HeatingController();
		ISensor sensor = new TemperatureSensor();
		IActuator actuator = new HeatingActuator();
		simulation.addRoom(room);
		room.getLocalContext().setTemperature(20.0D);
		room.getLocalContext().setPreference("targetTemperature", "21.5D");
		controller.attachSensor(sensor);
		controller.attachActuator(actuator);
		room.addActuator(actuator);
		room.addController(controller);
		room.addSensor(sensor);
		double temperature = room.getLocalContext().getTemperature();
		double previousTemperature = temperature;
		while (temperature < targetTemperature) {
			simulation.step();
			previousTemperature = temperature;
			temperature = room.getLocalContext().getTemperature();
			Assert.assertTrue(temperature >= previousTemperature);
		}
	}

	@Test
	public void testLightingChangeThroughActuator()
			throws SimulationContextException,
			SimulationConfigurationException, InterruptedException {
		double targetBrightness = 6000.0D;
		Simulation simulation = new Simulation(new SystemTimeTimeSource());
		simulation.setResolution(0);
		Room room = new Room();
		AbstractController controller = new LightingController();
		ISensor sensor = new LightSensor();
		IActuator[] lights = new IActuator[10];
		simulation.addRoom(room);
		simulation.getContext().setBrightness(5999.0D);
		room.getLocalContext().setPreference("targetBrightness",
				String.valueOf(targetBrightness));
		controller.attachSensor(sensor);
		for (int i = 0; i < 10; i++) {
			lights[i] = new LightingActuator();
			controller.attachActuator(lights[i]);
			room.addActuator(lights[i]);
		}
		room.addController(controller);
		room.addSensor(sensor);
		double brightness = room.getLocalContext().getBrightness();
		double previousBrightness = brightness;
		while (brightness < targetBrightness) {
			simulation.step();
			previousBrightness = brightness;
			brightness = room.getLocalContext().getBrightness();
			Assert.assertTrue(brightness > previousBrightness);
		}
	}

}
