package srsim.test.simulator;


import org.junit.Assert;
import org.junit.Test;

import srsim.actuators.HeatingActuator;
import srsim.controller.HeatingController;
import srsim.domain.Room;
import srsim.sensors.TemperatureSensor;
import srsim.simulator.SmartRoomSimulator;

public class SmartRoomSimulatorTest {

	@Test
	public void testStartStopSimulation() throws InterruptedException {
		SmartRoomSimulator simulator = new SmartRoomSimulator();
		simulator.startSimulation();
		Assert.assertTrue(simulator.isRunning());
		simulator.stopSimulation();
		Assert.assertFalse(simulator.isRunning());
	}
	
	@Test
	public void testAddRoom() {
		SmartRoomSimulator simulator = new SmartRoomSimulator();
		Room room = new Room();
		simulator.addRoom(room);
	}
	
	@Test
	public void testSetupAndRunSimulation() {
		SmartRoomSimulator simulator = new SmartRoomSimulator();
		Room room = new Room();
		simulator.addRoom(room);
		HeatingActuator heatingActuator = new HeatingActuator();
		TemperatureSensor temperatureSensor = new TemperatureSensor();
		HeatingController heatingController = new HeatingController();
		heatingController.attachActuator(heatingActuator);
		heatingController.attachSensor(temperatureSensor);
		room.addActuator(heatingActuator);
		room.addSensor(temperatureSensor);
		room.addController(heatingController);
		simulator.startSimulation();
	}
	
	

}
