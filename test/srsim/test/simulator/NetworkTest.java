package srsim.test.simulator;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.Socket;
import java.sql.SQLException;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import srsim.SRSimMain;
import srsim.simulator.SimulationConfigurationException;
import srsim.simulator.SimulationContextException;

public class NetworkTest {

	private static Thread srSimThread;
	private static SRSimMain srSimMain;

	@BeforeClass
	public static void setUp() throws InterruptedException {
		srSimThread = new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					srSimMain = new SRSimMain();
					srSimMain.start();
				} catch (InstantiationException | IllegalAccessException
						| ClassNotFoundException | IOException
						| SimulationContextException | SQLException
						| SimulationConfigurationException e) {
					e.printStackTrace();
				}
			}
		});
		srSimThread.start();
		while (srSimMain == null || !srSimMain.isReady()) {
			Thread.sleep(100);
		}
	}

	@Test
	public void testPreferencesServerDiscovery() throws IOException,
			InterruptedException {
		MulticastSocket multicastSocket = new MulticastSocket(3351);
		InetAddress mcastaddr = InetAddress.getByName("233.252.1.32");
		multicastSocket.joinGroup(mcastaddr);
		multicastSocket.setSoTimeout(3000);
		String msg = "SRSIMSRV";
		multicastSocket.send(new DatagramPacket(msg.getBytes(), msg.length(),
				mcastaddr, 3351));
		DatagramPacket p = new DatagramPacket(new byte[23], 23);
		do {
			multicastSocket.receive(p);
			msg = new String(p.getData());
		} while (msg.startsWith("SRSIMSRV"));
		multicastSocket.close();
		Assert.assertTrue(msg.startsWith("SRVADDR:"));
		String serverAddress = msg.split(":")[1].trim();
		String localHost = InetAddress.getLocalHost().getHostAddress();
		Assert.assertTrue(serverAddress.equals(localHost));
	}

	@Test
	public void testPreferencesServerCommunication() throws IOException,
			InterruptedException {
		Socket socket = new Socket(InetAddress.getLocalHost(), 5222);
		InputStreamReader reader = new InputStreamReader(
				socket.getInputStream());
		char[] buffer = new char[1024];
		socket.getOutputStream().write("{\"roomId\":\"TestRoom\"}".getBytes());
		int len = reader.read(buffer);
		Thread.sleep(2000);
		Assert.assertTrue(new String(buffer, 0, len).equals("OK"));
		socket.getOutputStream().write(
				"{\"targetBrightness\":\"6000.0\"}".getBytes());
		len = reader.read(buffer);
		Thread.sleep(1000);
		Assert.assertTrue(new String(buffer, 0, len).equals("OK"));
		socket.close();
	}

}
