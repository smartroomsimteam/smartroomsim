package srsim.sensors;

import java.util.concurrent.ThreadLocalRandom;

import srsim.domain.ISensor;
import srsim.simulator.SimulationContext;
import srsim.simulator.SimulationContextException;

/**
 * A simple temperature sensor.
 * 
 * @author phil
 * 
 */
public class TemperatureSensor implements ISensor {

	private SimulationContext context;
	private long id;
	
	public TemperatureSensor() {
		id = ThreadLocalRandom.current().nextLong();
	}

	@Override
	public double poll() throws SimulationContextException {
		if(context == null) {
			throw new SimulationContextException("Context undefined");
		}
		return context.getTemperature();
	}

	@Override
	public void step() {
		
	}

	@Override
	public void setContext(SimulationContext context) {
		this.context = context;
	}

	@Override
	public long getId() {
		return id;
	}

	@Override
	public void setId(long id) {
		this.id = id;
	}

}
