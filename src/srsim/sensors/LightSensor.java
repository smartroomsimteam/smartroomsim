package srsim.sensors;

import srsim.domain.ISensor;
import srsim.simulator.SimulationContext;
import srsim.simulator.SimulationContextException;

/**
 * Sensor class simulating a light sensor
 * 
 * @author phil
 * 
 */
public class LightSensor implements ISensor {

	private SimulationContext context;
	private long id;

	@Override
	public double poll() throws SimulationContextException {
		return context.getBrightness();
	}

	@Override
	public void step() {

	}

	@Override
	public void setContext(SimulationContext context) {
		this.context = context;
	}

	@Override
	public long getId() {
		return id;
	}

	@Override
	public void setId(long id) {
		this.id = id;
	}

}
