package srsim;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;

import srsim.persistence.DerbyEmbeddedDS;
import srsim.persistence.IDataStore;
import srsim.simulator.SimulationConfigurationException;
import srsim.simulator.SimulationContextException;
import srsim.simulator.SmartRoomSimulator;
import srsim.ui.IView;
import srsim.ui.console.ConsoleView;
import srsim.ui.query.QueryUI;
import srsim.ui.swing.SwingView;

/**
 * Smartroom simulator main application class.
 * 
 * @author phil
 * 
 */
public class SRSimMain {

	private static boolean nogui = false;
	private boolean ready;
	private SmartRoomSimulator simulator;
	private IDataStore dataStore;

	public SRSimMain() throws InstantiationException, IllegalAccessException,
			ClassNotFoundException, IOException, SimulationContextException,
			SQLException, SimulationConfigurationException {
		ready = false;
		dataStore = new DerbyEmbeddedDS();

		// Create SmartRoomSimulator and load configuration
		URL url = getClass().getClassLoader().getResource("simulation.json");
		simulator = new SmartRoomSimulator(dataStore);
		simulator.readConfiguration(url);
	}

	public boolean isReady() {
		return ready;
	}

	public static void main(String[] args) {
		try {
			if (args.length > 0) {
				for (String arg : args) {
					if (arg.equals("-nogui")) {
						nogui = true;
					} else if (arg.equals("-query")) {
						QueryUI.main(args);
						return;
					}
				}
			}
			SRSimMain srSimMain = new SRSimMain();
			srSimMain.start();
		} catch (InstantiationException | IllegalAccessException
				| ClassNotFoundException | IOException
				| SimulationContextException | SQLException
				| SimulationConfigurationException e) {
			e.printStackTrace();
		}
	}

	public void start() throws IOException {

		// Setup the preferences server
		simulator.listenForClients(5222);
		simulator.startSimulation();
		ready = true;

		// Show user interface
		IView view = nogui ? new ConsoleView(simulator) : new SwingView(
				simulator);
		view.init();
		view.display();
	}

}
