package srsim.persistence;

import java.util.List;

/**
 * Interface for data storage providers
 * 
 * @author phil
 * 
 */
public interface IDataStore {

	/**
	 * Stores a simulatioin record
	 * 
	 * @param record
	 */
	void persist(SimulationRecord record);

	/**
	 * Updates a persistent record with new values
	 * 
	 * @param record
	 */
	void update(SimulationRecord record);

	/**
	 * Deletes a persitent record
	 * 
	 * @param record
	 */
	void delete(SimulationRecord record);

	/**
	 * Retrieves a list ov simulation records matching the provided record
	 * 
	 * @param record
	 *            Simulation record specifying which records to retrieve
	 * @return
	 */
	List<SimulationRecord> find(Criterion criterion);

	/**
	 * Retrieves a list of all stored simulation records
	 * 
	 * @return
	 */
	List<SimulationRecord> findAll();

}
