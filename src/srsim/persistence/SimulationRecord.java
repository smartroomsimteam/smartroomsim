package srsim.persistence;

/**
 * Simulation record class representing a snapshot of the simulation context
 * meant for storage and evaluation
 * 
 * @author phil
 * 
 */
public class SimulationRecord {

	public static final double NOT_SET = -99.99D;
	private long id;
	private long timeStamp;
	private double temperature;
	private double brightness;
	private double energyConsumption;
	private int[] lightColor;
	private String musicGenre;
	private int musicVolume;
	private String roomId;

	public SimulationRecord() {
		id = 0L;
		timeStamp = 0L;
		temperature = NOT_SET;
		brightness = NOT_SET;
		energyConsumption = NOT_SET;
		lightColor = new int[] { -99, -99, -99 };
		musicGenre = null;
		musicVolume = -99;
	}

	public long getId() {
		return id;
	}

	public void setId(final long id) {
		this.id = id;
	}

	public long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(final long timeStamp) {
		this.timeStamp = timeStamp;
	}

	public double getTemperature() {
		return temperature;
	}

	public void setTemperature(final double temperature) {
		this.temperature = temperature;
	}

	public double getBrightness() {
		return brightness;
	}

	public void setBrightness(final double brightness) {
		this.brightness = brightness;
	}

	public double getEnergyConsumption() {
		return energyConsumption;
	}

	public void setEnergyConsumption(final double energyConsumption) {
		this.energyConsumption = energyConsumption;
	}
	
	public void setLightColor(final int[] lightColor) {
		this.lightColor = lightColor;
	}

	public int[] getLightColor() {
		return lightColor;
	}
	
	public void setMudicGenre(String musicGenre) {
		this.musicGenre = musicGenre;
	}

	public String getMusicGenre() {
		return musicGenre;
	}
	
	public void setMusicVolume(int musicVolume) {
		this.musicVolume = musicVolume;
	}

	public int getMusicVolume() {
		return musicVolume;
	}

	public void setRoomId(final String roomId) {
		this.roomId = roomId;
	}
	
	public String getRoomId() {
		return roomId;
	}
}
