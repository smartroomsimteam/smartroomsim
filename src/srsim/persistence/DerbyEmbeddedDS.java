package srsim.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

/**
 * Provides an IDataSore implementation based on the apache derby database.
 * 
 * @author phil
 * 
 */
public class DerbyEmbeddedDS implements IDataStore {

	private static final String JDBC_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
	private static final String PROTOCOL = "jdbc:derby:";
	private static final String DB_NAME = "srsim";
	private static final String TABLE_NAME = "simrec";

	private Connection connection;
	private PreparedStatement psInsertRecord;
	private PreparedStatement psUpdateRecord;
	private PreparedStatement psDeleteRecord;

	public DerbyEmbeddedDS() throws SQLException, InstantiationException,
			IllegalAccessException, ClassNotFoundException {
		Properties props = new Properties();
		Class.forName(JDBC_DRIVER).newInstance();
		connection = DriverManager.getConnection(PROTOCOL + DB_NAME
				+ ";create=true", props);
		if (!tableExists()) {
			createTable();
		}
		psInsertRecord = connection.prepareStatement("insert into "
				+ TABLE_NAME + " (timestamp, room, temperature, brightness,"
				+ " consumption, lightcolor, musicgenre, musicvolume)"
				+ " values (?,?,?,?,?,?,?,?)");
		psUpdateRecord = connection.prepareStatement("update " + TABLE_NAME
				+ " set timestamp=?, room='?', temperature=?, brightness=?,"
				+ " consumption=?, lightcolor='?', musicgenre='?',"
				+ " musicvolume=? where id=?");
		psDeleteRecord = connection.prepareStatement("delete from  "
				+ TABLE_NAME + " where id=?");
	}

	private boolean tableExists() throws SQLException {
		try {
			Statement s = connection.createStatement();
			s.execute("update " + TABLE_NAME
					+ " set temperature = 9.9 where 1=3");
		} catch (SQLException e) {
			String sqlState = e.getSQLState();
			if (sqlState.equals("42X05")) {
				return false;
			} else if (sqlState.equals("42X14") || sqlState.equals("42821")) {
				System.err.println("Erroneous table definition!");
				throw e;
			} else {
				System.err.println("Unhandled SQLException");
				throw e;
			}
		}
		return true;
	}

	private void createTable() throws SQLException {
		Statement stCreateTable = connection.createStatement();
		stCreateTable.execute("create table " + TABLE_NAME
				+ " (id int not null primary key "
				+ "generated always as identity "
				+ "(start with 1, increment by 1), "
				+ "timestamp bigint, room varchar(128), temperature double, "
				+ "brightness double, consumption double,"
				+ "lightcolor varchar(11), musicgenre varchar(128),"
				+ "musicvolume int)");
	}

	@Override
	public void persist(final SimulationRecord record) {
		try {
			psInsertRecord.setLong(1, record.getTimeStamp());
			psInsertRecord.setString(2, record.getRoomId());
			psInsertRecord.setDouble(3, record.getTemperature());
			psInsertRecord.setDouble(4, record.getBrightness());
			psInsertRecord.setDouble(5, record.getEnergyConsumption());
			int[] lightColor = record.getLightColor();
			psInsertRecord.setString(6, String.format("%d %d %d",
					lightColor[0], lightColor[1], lightColor[2]));
			psInsertRecord.setString(7, record.getMusicGenre());
			psInsertRecord.setInt(8, record.getMusicVolume());
			psInsertRecord.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<SimulationRecord> find(final Criterion criterion) {
		List<SimulationRecord> simulationRecords = new LinkedList<SimulationRecord>();
		try {
			String sql = "select * from " + TABLE_NAME + " where 1=1";
			if (criterion != null) {
				sql += " and " + criterion.toString();
			}
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				SimulationRecord simulationRecord = new SimulationRecord();
				simulationRecord.setRoomId(resultSet.getString("room"));
				simulationRecord.setBrightness(resultSet
						.getDouble("brightness"));
				simulationRecord.setEnergyConsumption(resultSet
						.getDouble("consumption"));
				simulationRecord.setId(resultSet.getInt("id"));
				simulationRecord.setTemperature(resultSet
						.getDouble("temperature"));
				simulationRecord.setTimeStamp(resultSet.getLong("timestamp"));
				String[] lightColor = resultSet.getString("lightcolor").split(
						" ");
				simulationRecord.setLightColor(new int[] {
						Integer.parseInt(lightColor[0]),
						Integer.parseInt(lightColor[1]),
						Integer.parseInt(lightColor[2]) });
				simulationRecord.setMudicGenre(resultSet
						.getString("musicgenre"));
				simulationRecord
						.setMusicVolume(resultSet.getInt("musicvolume"));
				simulationRecords.add(simulationRecord);
			}
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return simulationRecords;
	}

	@Override
	public List<SimulationRecord> findAll() {
		return find((Criterion) null);
	}

	@Override
	public void update(final SimulationRecord record) {
		if (record.getId() > 0) {
			try {
				psUpdateRecord.setLong(1, record.getTimeStamp());
				psUpdateRecord.setString(2, record.getRoomId());
				psUpdateRecord.setDouble(3, record.getTemperature());
				psUpdateRecord.setDouble(4, record.getBrightness());
				psUpdateRecord.setDouble(5, record.getEnergyConsumption());
				int[] lightColor = record.getLightColor();
				psUpdateRecord.setString(6, String.format("%d %d %d",
						lightColor[0], lightColor[1], lightColor[2]));
				psUpdateRecord.setString(7, record.getMusicGenre());
				psUpdateRecord.setInt(8, record.getMusicVolume());
				psUpdateRecord.execute();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void delete(final SimulationRecord record) {
		if (record.getId() > 0) {
			try {
				psDeleteRecord.setLong(1, record.getId());
				psDeleteRecord.execute();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}