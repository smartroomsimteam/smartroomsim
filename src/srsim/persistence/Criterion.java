package srsim.persistence;

public class Criterion {
	
	private StringBuilder queryBuilder;

	public Criterion(String value) {
		queryBuilder = new StringBuilder();
		queryBuilder.append(value);
	}
	
	public Criterion equal(String value) {
		queryBuilder.append(" = "+value);
		return this;
	}
	
	public Criterion notEqual(String value) {
		queryBuilder.append(" != "+value);
		return this;
	}
	
	public Criterion greaterThan(String value) {
		queryBuilder.append(" > "+value);
		return this;
	}
	
	public Criterion greaterEqual(String value) {
		queryBuilder.append(" >= "+value);
		return this;
	}
	
	public Criterion lessThan(String value) {
		queryBuilder.append(" < "+value);
		return this;
	}
	
	public Criterion lessEqual(String value) {
		queryBuilder.append(" <= "+value);
		return this;
	}
	
	public Criterion and(Criterion criterion) {
		queryBuilder.append(" and "+criterion.toString());
		return this;
	}
	
	public Criterion or(Criterion criterion) {
		queryBuilder.append(" or "+criterion.toString());
		return this;
	}
	
	@Override
	public String toString() {
		return queryBuilder.toString();
	}
}
