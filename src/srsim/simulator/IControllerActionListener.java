package srsim.simulator;

/**
 * Interface for controller action listeners
 * 
 * @author phil
 * 
 */
public interface IControllerActionListener {

	/**
	 * Called whenever a controller performs an action
	 * 
	 * @param event
	 */
	void handleControllerAction(ControllerActionEvent event);

}
