package srsim.simulator;

/**
 * Interface for a simulation listener
 * 
 * @author phil
 * 
 */
public interface ISimulationListener extends IContextListener {

	/**
	 * Called whenever a simulated controller takes an action
	 * 
	 * @param event
	 */
	void handleControllerAction(ControllerActionEvent event);

}
