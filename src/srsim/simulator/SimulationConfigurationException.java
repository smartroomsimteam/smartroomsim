package srsim.simulator;

public class SimulationConfigurationException extends Exception {

	public SimulationConfigurationException(String message) {
		super(message);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 6098668326754809524L;

}
