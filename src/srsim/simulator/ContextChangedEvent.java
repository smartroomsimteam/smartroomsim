package srsim.simulator;

/**
 * Context change event used to inform listeners about context changes
 * 
 * @author phil
 * 
 */
public class ContextChangedEvent {

	private long timeStamp;
	private SimulationContext context;

	public ContextChangedEvent(SimulationContext context)
			throws SimulationContextException {
		this.context = context;
		setTimeStamp(context.getTimeSource().getTimeStamp());
	}

	public long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}

	public SimulationContext getContext() {
		return context;
	}

}
