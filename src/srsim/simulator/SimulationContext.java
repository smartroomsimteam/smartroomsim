package srsim.simulator;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import srsim.domain.Room;

/**
 * The simulation context represents the current state of the simulated
 * environment.
 * 
 * @author phil
 * 
 */
public class SimulationContext {

	private static final double NOT_INITIALIZED = -99.99D;

	public static final String BRIGHTNESS = "brightness";
	public static final String TEMPERATURE = "temperature";
	public static final String LIGHTCOLOR = "lightColor";
	public static final String ENERGY_CONSUMPTION = "energyConsumption";
	public static final String MUSIC_GENRE = "musicGenre";
	public static final String MUSIC_VOLUME = "musicVolume";

	private double temperature;
	private double brightness;
	private double energyConsumption;
	private int[] lightColor;
	private String musicGenre;
	private int musicVolume;
	private ITimeSource timeSource;
	private Map<String, List<String>> preferences;
	private List<IContextListener> contextListeners;
	private SimulationContext parentContext;
	private List<String> presentSubjects;

	private final Room room;

	private void notifyContextListeners() throws SimulationContextException {
		for (IContextListener listener : contextListeners) {
			ContextChangedEvent event = new ContextChangedEvent(this);
			listener.handleContextChange(event);
		}
	}

	private boolean isInitialized(double value) {
		return value != NOT_INITIALIZED;
	}

	private void init() {
		preferences = new HashMap<String, List<String>>();
		contextListeners = new LinkedList<IContextListener>();
		presentSubjects = new LinkedList<String>();
		temperature = NOT_INITIALIZED;
		brightness = NOT_INITIALIZED;
		energyConsumption = 0.0D;
		lightColor = new int[] { 0, 0, 0 };
		musicGenre = "pop";
		musicVolume = 0;
	}

	public SimulationContext(final ITimeSource timeSource) {
		this.timeSource = timeSource;
		room = null;
		init();
	}

	public SimulationContext(final Room room) {
		this.room = room;
		init();
	}

	/**
	 * Returns the current temperature value
	 * 
	 * @return
	 * @throws SimulationContextException
	 */
	public double getTemperature() throws SimulationContextException {
		if (!isInitialized(temperature)) {
			if (parentContext != null) {
				temperature = parentContext.getTemperature();
			} else {
				throw new SimulationContextException("Value not initialized");
			}
		}
		return temperature;
	}

	/**
	 * Sets the current temperature value
	 * 
	 * @param temperature
	 * @throws SimulationContextException
	 */
	public void setTemperature(final double temperature)
			throws SimulationContextException {
		if (temperature != this.temperature) {
			this.temperature = temperature;
			notifyContextListeners();
		}
	}

	/**
	 * Returns the current brightness value
	 * 
	 * @return
	 * @throws SimulationContextException
	 */
	public double getBrightness() throws SimulationContextException {
		if (!isInitialized(brightness)) {
			if (parentContext != null) {
				brightness = parentContext.getBrightness();
			} else {
				throw new SimulationContextException("Value not initialized");
			}
		}
		return brightness;
	}

	/**
	 * Sets the current brightness value
	 * 
	 * @param brightness
	 * @throws SimulationContextException
	 */
	public void setBrightness(final double brightness)
			throws SimulationContextException {
		if (brightness != this.brightness) {
			this.brightness = brightness;
			notifyContextListeners();
		}
	}

	/**
	 * Returns the timesource used by the simulated environment to generate
	 * timestamps
	 * 
	 * @return
	 * @throws SimulationContextException
	 */
	public ITimeSource getTimeSource() throws SimulationContextException {
		if (timeSource == null) {
			throw new SimulationContextException("No timesource defined");
		}
		return timeSource;
	}

	/**
	 * Adds a listener to be notified whenever a context value changes
	 * 
	 * @param contextListener
	 */
	public void addContextListener(final IContextListener contextListener) {
		contextListeners.add(contextListener);
	}

	/**
	 * Sets the timesource to be used to generate timestamps
	 * 
	 * @param timeSource
	 * @throws SimulationContextException
	 */
	public void setTimeSource(final ITimeSource timeSource)
			throws SimulationContextException {
		if (parentContext != null) {
			ITimeSource parentTimesource = null;
			try {
				parentTimesource = parentContext.getTimeSource();
			} catch (SimulationContextException e) {
				parentTimesource = timeSource;
				parentContext.setTimeSource(timeSource);
			}
			if (parentTimesource != timeSource) {
				throw new SimulationContextException(
						"Context timesource is already "
								+ "defined by parent context");
			}
		}
		this.timeSource = timeSource;
	}

	/**
	 * Sets a preference for the simulated environment
	 * 
	 * @param key
	 *            The name of the preference to set
	 * @param value
	 *            The value for the preference to set
	 */
	public void setPreference(final String key, final String value) {
		synchronized (preferences) {
			if (!preferences.containsKey(key)) {
				preferences.put(key, new LinkedList<String>());
			}
			preferences.get(key).add(value);
		}
	}

	/**
	 * Removes the given preference value
	 * 
	 * @param key
	 *            The name of the preference from which to remove the value
	 * @param value
	 *            The value to remove from the specified preference
	 */
	public void unsetPreference(final String key, final String value) {
		synchronized (preferences) {
			if (preferences.containsKey(key)
					&& preferences.get(key).contains(value)) {
				preferences.get(key).remove(value);
			}
			if (preferences.containsKey(key) && preferences.get(key).isEmpty()) {
				preferences.remove(key);
			}
		}
	}

	/**
	 * Returns the values for a given preference
	 * 
	 * @param key
	 * @return
	 * @throws SimulationContextException
	 */
	public List<String> getPreference(final String key)
			throws SimulationContextException {
		synchronized (preferences) {
			if (preferences.containsKey(key)) {
				return preferences.get(key);
			} else if (parentContext != null
					&& parentContext.getPreferences().containsKey(key)) {
				return parentContext.getPreference(key);
			} else {
				return null;
			}
		}
	}

	/**
	 * Returns a list of all preferences
	 * 
	 * @return
	 * @throws SimulationContextException
	 */
	public Map<String, List<String>> getPreferences()
			throws SimulationContextException {
		synchronized (preferences) {
			return preferences;
		}
	}

	/**
	 * Sets a parent context
	 * 
	 * @param context
	 * @throws SimulationContextException
	 */
	public void setParentContext(final SimulationContext context)
			throws SimulationContextException {
		parentContext = context;
		timeSource = parentContext.getTimeSource();
	}

	/**
	 * Returns the current energy consumption value
	 * 
	 * @return
	 * @throws SimulationContextException
	 */
	public double getEnergyConsumption() throws SimulationContextException {
		if (!isInitialized(energyConsumption)) {
			if (parentContext != null) {
				energyConsumption = parentContext.getEnergyConsumption();
			} else {
				throw new SimulationContextException("Value not initialized");
			}
		}
		return energyConsumption;
	}

	/**
	 * Sets the current energy consumtion value
	 * 
	 * @param energyConsumption
	 * @throws SimulationContextException
	 */
	public void setEnergyConsumption(double energyConsumption)
			throws SimulationContextException {
		if (parentContext != null) {
			double totalConsumption = parentContext.getEnergyConsumption();
			totalConsumption = totalConsumption + energyConsumption;
			if (this.energyConsumption > 0.0D) {
				totalConsumption -= this.energyConsumption;
			}
			parentContext.setEnergyConsumption(totalConsumption);
		}
		this.energyConsumption = energyConsumption;
		notifyContextListeners();
	}

	/**
	 * Increases the energy consumption by a given amount
	 * 
	 * @param amount
	 * @throws SimulationContextException
	 */
	public void increaseEnergyConsumption(final double amount)
			throws SimulationContextException {
		if (parentContext != null) {
			parentContext.increaseEnergyConsumption(amount);
		}
		if (isInitialized(energyConsumption)) {
			energyConsumption += amount;
			notifyContextListeners();
		}
	}

	/**
	 * Decreases the energy consumtion by a given amount
	 * 
	 * @param amount
	 * @throws SimulationContextException
	 */
	public void decreaseEnergyConsumption(final double amount)
			throws SimulationContextException {
		if (parentContext != null) {
			parentContext.decreaseEnergyConsumption(amount);
		}
		if (energyConsumption >= amount) {
			energyConsumption -= amount;
			notifyContextListeners();
		}
	}

	/**
	 * Increases the temperature by a given amount
	 * 
	 * @param amount
	 * @throws SimulationContextException
	 */
	public void increaseTemperature(final double amount)
			throws SimulationContextException {
		if (isInitialized(temperature)) {
			temperature += amount;
			notifyContextListeners();
		}
	}

	/**
	 * Decreases the temperature by a given amount
	 * 
	 * @param amount
	 * @throws SimulationContextException
	 */
	public void decreaseTemperature(final double amount)
			throws SimulationContextException {
		if (temperature >= amount) {
			temperature -= amount;
			notifyContextListeners();
		}
	}

	/**
	 * Increases the brightness by a given amount
	 * 
	 * @param amount
	 * @throws SimulationContextException
	 */
	public void increaseBrightness(final double amount)
			throws SimulationContextException {
		if (isInitialized(brightness)) {
			brightness += amount;
			notifyContextListeners();
		}
	}

	/**
	 * Decreases the brightness by a given amount
	 * 
	 * @param amount
	 * @throws SimulationContextException
	 */
	public void decreaseBrightness(final double amount)
			throws SimulationContextException {
		if (brightness >= amount) {
			brightness -= amount;
			notifyContextListeners();
		}
	}

	/**
	 * Sets the given subject as present in the room associated with this
	 * context
	 * 
	 * @param subject
	 * @throws SimulationContextException
	 */
	public void setPresence(final String subject)
			throws SimulationContextException {
		presentSubjects.add(subject);
		notifyContextListeners();
	}

	/**
	 * Returns a list of subjects present in this room
	 * 
	 * @return
	 */
	public List<String> getPresentSubjects() {
		return presentSubjects;
	}

	/**
	 * Sets the given subject as no longer present in the room associated with
	 * this context.
	 * 
	 * @param subject
	 * @throws SimulationContextException
	 */
	public void unsetPresence(final String subject)
			throws SimulationContextException {
		if (presentSubjects.contains(subject)) {
			presentSubjects.remove(subject);
			notifyContextListeners();
		}
	}

	/**
	 * Returns the room this context is associated with
	 * 
	 * @return
	 */
	public Room getRoom() {
		return room;
	}

	/**
	 * Modifies the light color
	 * 
	 * @param lightColor
	 *            any number of rgb colors to add to the current color value
	 * @throws SimulationContextException
	 */
	public void modifyLightColor(final int[]... lightColor)
			throws SimulationContextException {
		int[] currentColor = new int[3];
		currentColor[0] = this.lightColor[0];
		currentColor[1] = this.lightColor[1];
		currentColor[2] = this.lightColor[2];
		for (int[] color : lightColor) {
			addLightColor(color);
		}
		if (currentColor[0] != this.lightColor[0]
				|| currentColor[1] != this.lightColor[1]
				|| currentColor[2] != this.lightColor[2]) {
			notifyContextListeners();
		}
	}

	/**
	 * Sets the color to the sum of the current and the given color with a
	 * maximum of 255 255 255
	 * 
	 * @param lightColor
	 */
	private void addLightColor(final int[] lightColor) {
		int red = this.lightColor[0] + lightColor[0];
		int green = this.lightColor[1] + lightColor[1];
		int blue = this.lightColor[2] + lightColor[2];
		red = red > 0 ? red : 0;
		green = green > 0 ? green : 0;
		blue = blue > 0 ? blue : 0;
		this.lightColor[0] = red < 255 ? red : 255;
		this.lightColor[1] = green < 255 ? green : 255;
		this.lightColor[2] = blue < 255 ? blue : 255;
	}

	/**
	 * Returns the current light color
	 * 
	 * @return
	 */
	public int[] getLightColor() {
		return lightColor;
	}

	/**
	 * Sets the light color to the given value
	 * 
	 * @param color
	 * @throws SimulationContextException
	 */
	public void setLightColor(final int[] color)
			throws SimulationContextException {
		if (lightColor[0] != color[0] || lightColor[1] != color[1]
				|| lightColor[2] != color[2]) {
			lightColor = new int[]{color[0], color[1], color[2]};
			notifyContextListeners();
		}
	}

	/**
	 * Sets the music genre currently played in this context's associated room
	 * 
	 * @param musicGenre
	 * @throws SimulationContextException
	 */
	public void setMusicGenre(final String musicGenre)
			throws SimulationContextException {
		if (!musicGenre.equals(this.musicGenre)) {
			this.musicGenre = musicGenre;
			notifyContextListeners();
		}
	}

	/**
	 * Returns the music genre currently played in this context's associated
	 * room
	 * 
	 * @return
	 */
	public String getMusicGenre() {
		return musicGenre;
	}

	/**
	 * Sets the volume of the music being played in this context's associated
	 * room
	 * 
	 * @param musicVolume
	 * @throws SimulationContextException
	 */
	public void setMusicVolume(final int musicVolume)
			throws SimulationContextException {
		if (musicVolume != this.musicVolume && musicVolume >= 0) {
			this.musicVolume = musicVolume;
			notifyContextListeners();
		}
	}

	/**
	 * returns the volume of the music being played in this context's associated
	 * room
	 * 
	 * @return
	 */
	public int getMusicVolume() {
		return musicVolume;
	}
}