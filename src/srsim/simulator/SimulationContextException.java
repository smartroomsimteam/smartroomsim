package srsim.simulator;

/**
 * Exception thrown by several Simulation related classes
 * 
 * @author phil
 * 
 */
public class SimulationContextException extends Exception {

	public SimulationContextException(String message) {
		super(message);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 2902999260100640034L;

}
