package srsim.simulator;

/**
 * Interface for context change listener
 * 
 * @author phil
 * 
 */
public interface IContextListener {

	/**
	 * Called whenever the simulation context is modified
	 * 
	 * @param event
	 * @throws SimulationContextException
	 */
	void handleContextChange(ContextChangedEvent event)
			throws SimulationContextException;

}
