package srsim.simulator;

/**
 * ITimesource implementation using System.currentTimeMillis() to generate
 * timestamps
 * 
 * @author phil
 * 
 */
public class SystemTimeTimeSource implements ITimeSource {

	@Override
	public long getTimeStamp() {
		return System.currentTimeMillis();
	}

}
