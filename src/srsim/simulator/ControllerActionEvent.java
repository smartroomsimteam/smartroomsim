package srsim.simulator;

import srsim.domain.IController;

/**
 * Event class used to inform listeners about controller actions
 * 
 * @author phil
 * 
 */
public class ControllerActionEvent {

	private String message;
	private long timeStamp;
	private IController source;

	public ControllerActionEvent(String message, long timeStamp, IController source) {
		this.message = message;
		this.timeStamp = timeStamp;
		this.source = source;
	}

	public String getMessage() {
		return message;
	}

	public long getTimeStamp() {
		return timeStamp;
	}
	
	public IController getSource() {
		return source;
	}
}
