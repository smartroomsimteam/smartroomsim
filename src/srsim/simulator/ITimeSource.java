package srsim.simulator;

/**
 * Interface for timsource implementations used by the simulation to generate
 * timestamps
 * 
 * @author phil
 * 
 */
public interface ITimeSource {

	long getTimeStamp();

}
