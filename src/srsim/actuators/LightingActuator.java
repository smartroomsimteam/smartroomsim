package srsim.actuators;

import java.util.concurrent.ThreadLocalRandom;

import srsim.domain.IActuator;
import srsim.simulator.SimulationContext;
import srsim.simulator.SimulationContextException;

/**
 * Actuator class simulation a simple lightsource such as a lamp.
 * 
 * @author phil
 * 
 */
public class LightingActuator implements IActuator {

	private static final double INITIAL_CONSUMPTION = 8.0D;
	private static final double CONSUMPTION_STEP = 1.0D;
	private static final double INITIAL_BRIGHTNESS = 400.0D;
	private static final double BRIGHTNESS_STEP = 100.0D;
	private static final double MAX_BRIGHTNESS = 800.0D;
	private static final double MIN_BRIGHTNESS = 100.0D;

	private double consumption;
	private double brightness;
	private long id;
	private int state;
	private SimulationContext context;
	private int[] targetLightColor;

	public LightingActuator() {
		consumption = 0.0D;
		brightness = 0.0D;
		targetLightColor = new int[] { 0, 0, 0 };
		id = ThreadLocalRandom.current().nextLong();
		state = IDLE;
	}

	@Override
	public int getState() {
		return state;
	}

	@Override
	public void enable() throws SimulationContextException {
		state = IActuator.ACTIVE;
		if (brightness < INITIAL_BRIGHTNESS) {
			brightness = INITIAL_BRIGHTNESS;
			consumption = INITIAL_CONSUMPTION;
		}
		context.increaseBrightness(brightness);
		context.increaseEnergyConsumption(consumption);
	}

	@Override
	public void disable() throws SimulationContextException {
		state = IActuator.IDLE;
		context.decreaseBrightness(brightness);
		context.decreaseEnergyConsumption(consumption);
	}

	@Override
	public void step() throws SimulationContextException {

	}

	@Override
	public void setContext(final SimulationContext context) {
		this.context = context;
	}

	@Override
	public long getId() {
		return id;
	}

	@Override
	public void setId(final long id) {
		this.id = id;
	}

	@Override
	public double getEnergyConsumption() {
		return state == IActuator.ACTIVE ? consumption : 0.0D;
	}

	@Override
	public void turnUp() throws SimulationContextException {
		if (brightness < MAX_BRIGHTNESS) {
			brightness += BRIGHTNESS_STEP;
			consumption += CONSUMPTION_STEP;
			context.increaseBrightness(BRIGHTNESS_STEP);
			context.increaseEnergyConsumption(CONSUMPTION_STEP);
		}
	}

	@Override
	public void turnDown() throws SimulationContextException {
		if (brightness > MIN_BRIGHTNESS) {
			brightness -= BRIGHTNESS_STEP;
			consumption -= CONSUMPTION_STEP;
			context.decreaseBrightness(BRIGHTNESS_STEP);
			context.decreaseEnergyConsumption(CONSUMPTION_STEP);
		}
	}

	/**
	 * Sets the light color for this lighting actuator. Returns true if the
	 * given color is supported and false otherwise.
	 * 
	 * @param targetLightColor
	 *            target color array [0]->red, [1]->blue, [2]->green
	 * @throws SimulationContextException
	 */
	public boolean setColor(final int[] targetLightColor)
			throws SimulationContextException {
		if (isSupported(targetLightColor)
				&& !colorsMatch(targetLightColor, this.targetLightColor)) {
			int[] inverseColor = new int[3];
			inverseColor[0] = -this.targetLightColor[0];
			inverseColor[1] = -this.targetLightColor[1];
			inverseColor[2] = -this.targetLightColor[2];
			this.targetLightColor = targetLightColor;
			context.modifyLightColor(inverseColor, targetLightColor);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns true if colorA and colorB contain the same values
	 * 
	 * @param colorA
	 * @param colorB
	 * @return
	 */
	private static boolean colorsMatch(final int[] colorA, final int[] colorB) {
		return colorA[0] == colorB[0] && colorA[1] == colorB[1]
				&& colorA[2] == colorB[2];
	}

	/**
	 * Returns true if the given color is supported by this light.
	 * 
	 * @param color
	 * @return
	 */
	private boolean isSupported(int[] color) {
		return true;
	}

	/**
	 * Returns the maximum brightness this light can produce
	 * 
	 * @return
	 */
	public double getMaxBrightness() {
		return MAX_BRIGHTNESS;
	}

	/**
	 * Returns the current brightness
	 * 
	 * @return
	 */
	public double getBrightness() {
		return brightness;
	}

	public double getMinimumBrightness() {
		return MIN_BRIGHTNESS;
	}

}
