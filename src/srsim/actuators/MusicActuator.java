package srsim.actuators;

import srsim.domain.IActuator;
import srsim.simulator.SimulationContext;
import srsim.simulator.SimulationContextException;

/**
 * Music actuator simulation a simple playback device.
 * 
 * @author phil
 * 
 */
public class MusicActuator implements IActuator {

	private static final double POWER_CONSUMPTION = 75.0D;
	private static final int VOLUME_STEP_WIDTH = 5;
	private static final int MAX_VOLUME = 100;
	private int state;
	private String genre;
	private SimulationContext context;
	private long id;
	private int volume;

	public MusicActuator() {
		genre = "pop";
		state = IActuator.IDLE;
		context = null;
	}

	@Override
	public int getState() {
		return state;
	}

	@Override
	public void enable() throws SimulationContextException {
		if (state != IActuator.ACTIVE) {
			state = IActuator.ACTIVE;
		}
	}

	@Override
	public void disable() throws SimulationContextException {
		if (state == IActuator.ACTIVE) {
			state = IActuator.IDLE;
		}
	}

	@Override
	public void step() throws SimulationContextException {

	}

	@Override
	public void setContext(final SimulationContext context) {
		this.context = context;
	}

	@Override
	public long getId() {
		return id;
	}

	@Override
	public void setId(final long id) {
		this.id = id;
	}

	@Override
	public double getEnergyConsumption() {
		return state == IActuator.ACTIVE ? POWER_CONSUMPTION : 0.0D;
	}

	@Override
	public void turnUp() throws SimulationContextException {
		if (volume < MAX_VOLUME) {
			volume += VOLUME_STEP_WIDTH;
			context.setMusicVolume(volume);
		}
	}

	@Override
	public void turnDown() throws SimulationContextException {
		if (volume > 0) {
			volume -= VOLUME_STEP_WIDTH;
			context.setMusicVolume(volume);
		}
	}

	/**
	 * Sets the music genre to be played by this playback device
	 * 
	 * @param selectedGenre
	 * @throws SimulationContextException
	 */
	public void setGenre(final String genre) throws SimulationContextException {
		if (!genre.equals(this.genre)) {
			this.genre = genre;
			context.setMusicGenre(genre);
		}
	}

	/**
	 * Returns the music genre currently played by this playback device
	 * 
	 * @return
	 */
	public String getGenre() {
		return genre;
	}

	/**
	 * Sets the volume of the music currently playing to a given value
	 * 
	 * @param volume
	 * @throws SimulationContextException 
	 */
	public void setVolume(final int volume) throws SimulationContextException {
		if (volume != this.volume && volume >= 0 && volume <= MAX_VOLUME) {
			this.volume = volume;
			context.setMusicVolume(volume);
		}
	}

	/**
	 * Returns the volume of the music currently played by this playback device
	 * 
	 * @return
	 */
	public int getVolume() {
		return volume;
	}

}
