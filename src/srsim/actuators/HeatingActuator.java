package srsim.actuators;

import java.util.concurrent.ThreadLocalRandom;

import srsim.domain.IActuator;
import srsim.simulator.SimulationContext;
import srsim.simulator.SimulationContextException;

/**
 * A simple heating actuator 
 * 
 * @author phil
 * 
 */
public class HeatingActuator implements IActuator {

	private static final double[] ACTIVE_ENERGY_CONSUMPTION = new double[] {
			2600.0D, 2800.0D, 3000.0D, 3200.0D, 3400.0D };
	private static final double[] HEATING_STEPS = new double[] { 0.1D, 0.2D,
			0.3D, 0.4D, 0.5D };
	private int currentLevel = 0;
	private int state;
	private SimulationContext context;
	private long id;

	public HeatingActuator() {
		state = IDLE;
		id = ThreadLocalRandom.current().nextLong();
	}

	@Override
	public double getEnergyConsumption() {
		switch (state) {
		case ACTIVE:
			return ACTIVE_ENERGY_CONSUMPTION[0];
		case IDLE:
		default:
			return 0.0D;
		}
	}

	@Override
	public int getState() {
		return state;
	}

	@Override
	public void enable() throws SimulationContextException {
		if (state != ACTIVE) {
			state = ACTIVE;
			context.increaseEnergyConsumption(ACTIVE_ENERGY_CONSUMPTION[currentLevel]);
		}
	}

	@Override
	public void disable() throws SimulationContextException {
		if (state == ACTIVE) {
			state = IDLE;
			context.decreaseEnergyConsumption(ACTIVE_ENERGY_CONSUMPTION[currentLevel]);
		}
	}

	@Override
	public void step() throws SimulationContextException {
		if (context == null) {
			throw new SimulationContextException("Context undefined");
		} else {
			if (state == ACTIVE) {
				context.increaseTemperature(HEATING_STEPS[currentLevel]);
			}
		}
	}

	@Override
	public void setContext(SimulationContext context) {
		this.context = context;
	}

	@Override
	public long getId() {
		return id;
	}

	@Override
	public void setId(long id) {
		this.id = id;
	}

	@Override
	public void turnUp() throws SimulationContextException {
		if (currentLevel < HEATING_STEPS.length) {
			double delta = ACTIVE_ENERGY_CONSUMPTION[currentLevel + 1]
					- ACTIVE_ENERGY_CONSUMPTION[currentLevel];
			currentLevel++;
			context.increaseEnergyConsumption(delta);
		}
	}

	@Override
	public void turnDown() throws SimulationContextException {
		if (currentLevel > 0) {
			double delta = ACTIVE_ENERGY_CONSUMPTION[currentLevel]
					- ACTIVE_ENERGY_CONSUMPTION[currentLevel-1];
			currentLevel--;
			context.decreaseEnergyConsumption(delta);
		}
	}

}
