package srsim.controller;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import srsim.domain.AbstractController;
import srsim.domain.IActuator;
import srsim.domain.ISensor;
import srsim.simulator.SimulationContextException;

/**
 * Heating controller class simulating the behavior of a thermostate monitoring
 * the room temperature and activating/deactivating the heatinbg according to
 * the set target temperature.
 * 
 * @author phil
 * 
 */
public class HeatingController extends AbstractController {

	private IActuator actuator;
	private ISensor sensor;
	private double targetTemperature;

	public HeatingController() {
		targetTemperature = 0.0D;
		id = ThreadLocalRandom.current().nextLong();
	}

	@Override
	public void attachSensor(final ISensor sensor) {
		this.sensor = sensor;
	}

	@Override
	public void attachActuator(final IActuator actuator) {
		this.actuator = actuator;
	}

	@Override
	public void step() throws SimulationContextException {
		updatePreferences();
		double temperature = sensor.poll();
		if (temperature < targetTemperature
				&& actuator.getState() != IActuator.ACTIVE) {
			notifyListeners("Temperature too low, enabling "
					+ "heating (target temperature is " + targetTemperature
					+ " �C)");
			actuator.enable();
		} else if (temperature >= targetTemperature
				&& actuator.getState() == IActuator.ACTIVE) {
			notifyListeners("Target temperature reached, disabling heating.");
			actuator.disable();
		}
	}

	/**
	 * Obtains latest preferences from the simulation context
	 * @throws SimulationContextException 
	 */
	private void updatePreferences() throws SimulationContextException {
		List<String> preferences = context.getPreference("targetTemperature");
		if (preferences != null) {
			targetTemperature = 0.0D;
			for (String preference : preferences) {
				targetTemperature += Double.parseDouble(preference);
			}
			targetTemperature /= preferences.size();
		}
	}

	@Override
	public List<ISensor> getAttachedSensors() {
		List<ISensor> sensors = new LinkedList<ISensor>();
		sensors.add(sensor);
		return sensors;
	}

	@Override
	public List<IActuator> getAttachedActuators() {
		List<IActuator> actuators = new LinkedList<IActuator>();
		actuators.add(actuator);
		return actuators;
	}
}
