package srsim.io;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;

import srsim.simulator.ISimulationController;
import srsim.simulator.SimulationContextException;

/**
 * Preferences server listening for client connections and processing them
 * asychronously
 * 
 * @author phil
 * 
 */
public class PreferencesServer extends Thread {

	private ServerSocket serverSocket;
	private boolean listening;
	private ISimulationController controller;
	private ServerAdvertiser advertiser;
	private List<ClientConnection> clientConnections;

	public PreferencesServer(final ISimulationController controller)
			throws IOException {
		this.controller = controller;
		advertiser = new ServerAdvertiser();
		clientConnections = new LinkedList<ClientConnection>();
		setName("SRSIM PreferencesServer");
	}

	@Override
	public void run() {
		while (listening) {
			try {
				Socket clientSocket = serverSocket.accept();
				clientConnections.add(new ClientConnection(clientSocket, this));
			} catch (IOException e) {
				continue;
			}
		}
	}

	/**
	 * Starts listening for clients
	 * 
	 * @param port
	 *            TCP port on which to listen for inbound connections
	 * @param advertise
	 *            Flag indicating whether the server should be advertised on the
	 *            local network
	 * @throws IOException
	 */
	public void listen(final int port, final boolean advertise)
			throws IOException {
		serverSocket = new ServerSocket(port);
		listening = true;
		if (advertise) {
			advertiser.start();
		}
		start();
	}

	/**
	 * Sets a prefrence value
	 * 
	 * @param key
	 * @param value
	 * @param value
	 * @throws SimulationContextException
	 */
	public void setPreference(final String roomId, final String key,
			final String value) throws SimulationContextException {
		controller.setPreference(roomId, key, value);
	}

	/**
	 * Unsets a prefrence value
	 * 
	 * @param key
	 * @param value
	 * @throws SimulationContextException
	 */
	public void unsetPreference(final String roomId, final String key,
			final String value) throws SimulationContextException {
		controller.unsetPreference(roomId, key, value);
	}

	/**
	 * Shuts down the presence server closing all open connections. If the
	 * server is advertising the server advertiser will be stopped aswell.
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 */
	public void shutDown() throws InterruptedException, IOException {
		if (listening) {
			listening = false;
			for (ClientConnection clientConnection : clientConnections) {
				clientConnection.close();
			}
			serverSocket.close();
			if (advertiser.isAdvertising()) {
				advertiser.shutDown();
			}
		}
	}

	/**
	 * Sets uses location information recieved from a client to provide presence
	 * information to the simulation controller
	 * 
	 * @param clientAddress
	 * @param roomId
	 */
	public void setClientLocation(final String clientAddress,
			final String roomId) {
		try {
			if (roomId == null) {
				controller.unsetPresence(clientAddress);
			} else {
				controller.setPresence(roomId, clientAddress);
			}
		} catch (SimulationContextException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Returns the simulation controller instance
	 * 
	 * @return
	 */
	public ISimulationController getController() {
		return controller;
	}
}
