package srsim.io;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;

/**
 * Server advertiser periodically broadcasting the server's address via UDP
 * multicast on the local network
 * 
 * @author phil
 * 
 */
public class ServerAdvertiser extends Thread {

	private static final String MULTICAST_ADDRESS = "233.252.1.32";
	private static final int MULTICAST_PORT = 3351;
	private boolean advertising;
	private String serverAddress;
	private MulticastSocket multicastSocket;

	public ServerAdvertiser() throws UnknownHostException {
		init(InetAddress.getLocalHost().getHostAddress());
	}

	public ServerAdvertiser(String serverAddress) {
		init(serverAddress);
	}

	private void init(String serverAddress) {
		this.serverAddress = serverAddress;
		setName("SRSIM ServerAdvertiser");
		setDaemon(true);
	}

	@Override
	public void run() {
		advertising = true;
		try {
			multicastSocket = new MulticastSocket(
					MULTICAST_PORT);
			InetAddress mcastaddr = InetAddress.getByName(MULTICAST_ADDRESS);
			multicastSocket.joinGroup(mcastaddr);
			while (advertising) {
				try {
					DatagramPacket p = new DatagramPacket(new byte[8], 8);
					multicastSocket.receive(p);
					if (new String(p.getData()).equals("SRSIMSRV")) {
						String reply = "SRVADDR:" + serverAddress;
						multicastSocket.send(new DatagramPacket(reply
								.getBytes(), reply.length(), mcastaddr,
								MULTICAST_PORT));
					}
				} catch (IOException e) {
					// Connection closed
				}
			}
			multicastSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void shutDown() throws InterruptedException {
		advertising = false;
		multicastSocket.close();
		join();
	}

	public boolean isAdvertising() {
		return advertising;
	}
}
