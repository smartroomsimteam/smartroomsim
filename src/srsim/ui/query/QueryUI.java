package srsim.ui.query;

import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import srsim.persistence.Criterion;
import srsim.persistence.DerbyEmbeddedDS;
import srsim.persistence.IDataStore;
import srsim.persistence.SimulationRecord;

public class QueryUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7598325094770245141L;
	private JPanel contentPane;
	private JTextField textField;

	/**
	 * Launch the application.
	 * 
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	public static void main(String[] args) throws InstantiationException,
			IllegalAccessException, ClassNotFoundException, SQLException {
		new QueryUI(new DerbyEmbeddedDS()).display();
	}

	public void display() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public QueryUI(final IDataStore dataStore) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 469, 330);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		textField = new JTextField();
		textField.setBounds(10, 11, 323, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		JButton btnSearch = new JButton("Search");

		btnSearch.setBounds(343, 10, 89, 23);
		contentPane.add(btnSearch);

		final JTextArea textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setBounds(10, 45, 728, 206);
		JScrollPane sp = new JScrollPane(textArea);
		sp.setBounds(10, 45, 414, 206);
		contentPane.add(sp);

		btnSearch.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				List<SimulationRecord> result = dataStore.find(new Criterion(
						textField.getText()));
				for (SimulationRecord record : result) {
					textArea.append(String.format("%tT ", record.getTimeStamp()));
					textArea.append(record.getRoomId());
					textArea.append(String.format(" %.2fW",
							record.getEnergyConsumption()));
					textArea.append(String.format(" %.2f deg C",
							record.getTemperature()));
					textArea.append(String.format(" %.2f lux",
							record.getBrightness()));
					textArea.append(" #");
					for (int c : record.getLightColor()) {
						textArea.append(String.format("%02X", c));
					}
					textArea.append(" " + record.getMusicGenre());
					textArea.append(String.format(" %d\n",
							record.getMusicVolume()));
				}
			}
		});
	}
}