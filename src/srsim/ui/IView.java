package srsim.ui;


public interface IView {

	void init();

	void display();

}
