package srsim.ui.console;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.List;

import srsim.domain.Room;
import srsim.simulator.ContextChangedEvent;
import srsim.simulator.ControllerActionEvent;
import srsim.simulator.ISimulationController;
import srsim.simulator.ISimulationListener;
import srsim.simulator.SimulationContextException;
import srsim.ui.IView;

/**
 * Simple console based interface for debugging purposes
 * 
 * @author phil
 * 
 */
public class ConsoleView implements IView, ISimulationListener {

	private ISimulationController controller;
	private boolean acceptInput;
	private boolean simulationActive;

	private void handleInput() throws InterruptedException, IOException {
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(System.in));
		String input = bufferedReader.readLine();
		if (input.equals("exit")) {
			acceptInput = false;
			if (simulationActive) {
				System.out.print(getTime(System.currentTimeMillis())
						+ " [SIMULATOR] Stopping simulation...");
				controller.stopSimulation();
				System.out.println("done.");
				simulationActive = false;
			}
			System.out.print(getTime(System.currentTimeMillis())
					+ " [SIMULATOR] Shutting down...");
			controller.shutDown();
			System.out.println("done.");
		} else if (input.startsWith("list ")) {
			if (input.endsWith(" rooms")) {
				List<Room> rooms = controller.getRooms();
				for (Room room : rooms) {
					System.out.println(room.getName());
				}
			}
		} else if (input.startsWith("add ")) {
			if (input.endsWith(" room")) {
				Room room = new Room();
				System.out.print("room name: ");
				input = bufferedReader.readLine();
				room.setName(input);
				controller.addRoom(room);
				System.out.println("Room " + input + " added.");
			}
		} else if (input.startsWith("start sim")) {
			System.out.print(getTime(System.currentTimeMillis())
					+ " [SIMULATOR] starting simulation...");
			controller.startSimulation();
			simulationActive = true;
			System.out.println("done.");
		} else if (input.startsWith("save sim")) {
			writeConfigFile();
		}
	}

	private void writeConfigFile() throws FileNotFoundException,
			UnsupportedEncodingException, IOException, MalformedURLException {
		try {
			String userHome = System.getProperty("user.home");
			String fileName = userHome + File.separator
					+ "unnamed_simulation.json";
			controller.writeConfiguration(fileName);
			System.out.println(getTime(System.currentTimeMillis())
					+ "[SIMULATOR] Configuration saved to " + fileName);
		} catch (SimulationContextException e) {
			e.printStackTrace();
		}
	}

	private String getTime(long timeStamp) {
		return String.format("%tT", timeStamp);
	}

	public ConsoleView(ISimulationController controller) {
		this.controller = controller;
		simulationActive = false;
		controller.addSimulationListener(this);
		System.out.println("-------------------------------------");
		System.out.println("SmartRoomSimulator");
		System.out.println("-------------------------------------");
	}

	@Override
	public void init() {
		acceptInput = false;
	}

	@Override
	public void display() {
		acceptInput = true;
		try {
			while (acceptInput) {
				handleInput();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void handleContextChange(ContextChangedEvent event)
			throws SimulationContextException {
		String time = getTime(event.getTimeStamp());
		System.out.print(time + " [CONTEXT_CHANGE]");
		Room room = event.getContext().getRoom();
		System.out
				.print("[" + (room != null ? room.getName() : "GLOBAL") + "]");
		System.out.printf(" Temperature is %.2f deg C", event.getContext()
				.getTemperature());
		System.out.printf(" | Brightness is %.2f", event.getContext()
				.getBrightness());
		System.out.print(" | Light-color is #");
		for (int c : event.getContext().getLightColor()) {
			System.out.printf(String.format("%02X", c));
		}
		int volume = event.getContext().getMusicVolume();
		System.out.print(" | Playing "
				+ (volume > 0 ? event.getContext().getMusicGenre() : "no")
				+ " music");
		if (volume > 0) {
			System.out.printf(" (volume: %d)", volume);
		}
		System.out.printf(" | Total energy consumption is %.2fW", event
				.getContext().getEnergyConsumption());
		System.out.print(" | ");
		if (event.getContext().getPresentSubjects().isEmpty()) {
			System.out.println("no presence detected");
		} else {
			System.out.println("presence detected");
		}
	}

	@Override
	public void handleControllerAction(ControllerActionEvent event) {
		long timeStamp = event.getTimeStamp();
		System.out.print(getTime(timeStamp) + " [CONTOLLER_ACTION]");
		System.out.printf("[%s] ", event.getSource().getContext().getRoom()
				.getName());
		System.out.println(event.getMessage());
	}

}
