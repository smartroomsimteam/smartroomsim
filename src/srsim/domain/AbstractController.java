package srsim.domain;

import java.util.LinkedList;
import java.util.List;

import srsim.simulator.ControllerActionEvent;
import srsim.simulator.IControllerActionListener;
import srsim.simulator.SimulationConfigurationException;
import srsim.simulator.SimulationContext;
import srsim.simulator.SimulationContextException;

/**
 * Abstract base class for controller inplementations
 * 
 * @author phil
 * 
 */
public abstract class AbstractController implements IController {

	private List<IControllerActionListener> actionListeners = new LinkedList<IControllerActionListener>();
	protected SimulationContext context;
	protected long id;

	@Override
	public abstract void attachSensor(ISensor sensor)
			throws SimulationConfigurationException;

	@Override
	public abstract void attachActuator(IActuator actuator)
			throws SimulationConfigurationException;

	@Override
	public abstract void step() throws SimulationContextException;

	@Override
	public final void addControllerActionListener(
			final IControllerActionListener listener) {
		actionListeners.add(listener);
	}

	@Override
	public final void setContext(final SimulationContext context) {
		this.context = context;
	}

	@Override
	public final SimulationContext getContext() {
		return context;
	}

	@Override
	public final long getId() {
		return id;
	}

	@Override
	public final void setId(final long id) {
		this.id = id;
	}

	@Override
	public abstract List<ISensor> getAttachedSensors();

	@Override
	public abstract List<IActuator> getAttachedActuators();

	/**
	 * Notifies attached listeners whenever the controller changes an actuator
	 * state
	 * 
	 * @param message
	 * @throws SimulationContextException
	 */
	protected final void notifyListeners(final String message)
			throws SimulationContextException {
		ControllerActionEvent event = new ControllerActionEvent(message,
				context.getTimeSource().getTimeStamp(), this);
		synchronized (actionListeners) {
			for (IControllerActionListener listener : actionListeners) {
				listener.handleControllerAction(event);
			}
		}
	}

}
