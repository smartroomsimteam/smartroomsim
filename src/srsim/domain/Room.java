package srsim.domain;

import java.util.LinkedList;
import java.util.List;

import srsim.simulator.SimulationContext;
import srsim.simulator.SimulationContextException;

/**
 * The room class holds the configuratiuon of the simulated room consisting of
 * geometry, sensors, controllers and actuators. It also acts as local simulation
 * context for all its sensors and actuators.
 * 
 * @author phil
 * 
 */
public class Room {

	private long id;
	private double length;
	private double width;
	private double height;
	private double x;
	private double y;

	private List<ISensor> sensors;
	private List<IActuator> actuators;
	private List<IController> controllers;
	private SimulationContext context;
	private String name;

	public Room() {
		sensors = new LinkedList<ISensor>();
		actuators = new LinkedList<IActuator>();
		controllers = new LinkedList<IController>();
		context = new SimulationContext((Room)this);
		id = 0L;
	}

	public void setContext(SimulationContext context)
			throws SimulationContextException {
		this.context.setParentContext(context);
	}

	public void setDimensions(double length, double width, double height) {
		this.setLength(length);
		this.setWidth(width);
		this.setHeight(height);
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public void addSensor(ISensor sensor) {
		sensor.setContext(context);
		sensors.add(sensor);
	}

	public List<ISensor> getSensors() {
		return sensors;
	}

	public List<IController> getControllers() {
		return controllers;
	}

	public void addActuator(IActuator actuator) {
		actuator.setContext(context);
		actuators.add(actuator);
	}

	public List<IActuator> getActuators() {
		return actuators;
	}

	public void addController(IController controller) {
		controller.setContext(context);
		controllers.add(controller);
	}

	public SimulationContext getLocalContext()
			throws SimulationContextException {
		if (context == null) {
			throw new SimulationContextException("Context undefined");
		}
		return context;
	}

	public void setPosition(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
}
