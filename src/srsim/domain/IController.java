package srsim.domain;

import java.util.List;

import srsim.simulator.IControllerActionListener;
import srsim.simulator.SimulationConfigurationException;
import srsim.simulator.SimulationContext;
import srsim.simulator.SimulationContextException;

/**
 * Interface for a simulated controll element. Controllers take sensor data,
 * derive an action from it and controll actuators acordingly.
 * 
 * @author phil
 * 
 */
public interface IController {

	/**
	 * Attaches a sensor to be monitored by this controller.
	 * 
	 * @param sensor
	 * @throws SimulationConfigurationException
	 */
	void attachSensor(ISensor sensor) throws SimulationConfigurationException;

	/**
	 * Attaches an actuator to be controlled by this controller.
	 * 
	 * @param actuator
	 * @throws SimulationConfigurationException
	 */
	void attachActuator(IActuator actuator)
			throws SimulationConfigurationException;

	/**
	 * Makes a simulation step.
	 * 
	 * @throws SimulationContextException
	 */
	void step() throws SimulationContextException;

	/**
	 * Adds a controller action listener to be notified whenever the controller
	 * takes action
	 * 
	 * @param listener
	 */
	void addControllerActionListener(IControllerActionListener listener);

	/**
	 * Sets the simulation context of the controller
	 * 
	 * @param context
	 */
	void setContext(SimulationContext context);

	/**
	 * Returns the id of the controller
	 * 
	 * @return
	 */
	long getId();

	/**
	 * Sets the id of the controller
	 * 
	 * @param id
	 */
	void setId(long id);

	/**
	 * Returns a list of sensors attached to the controller
	 * 
	 * @return
	 */
	List<ISensor> getAttachedSensors();

	/**
	 * Returns a list of actuators attached to the controller
	 * 
	 * @return
	 */
	List<IActuator> getAttachedActuators();

	/**
	 * Returns the simulation context for this controller
	 * 
	 * @return
	 */
	SimulationContext getContext();
}
