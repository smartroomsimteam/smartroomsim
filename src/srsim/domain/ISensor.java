package srsim.domain;

import srsim.simulator.SimulationContext;
import srsim.simulator.SimulationContextException;

/**
 * A generic sensor interface for simulated sensors.
 * 
 * @author phil
 * 
 */
public interface ISensor {

	/**
	 * Returns the current vlaue from the sensor.
	 * 
	 * @return
	 * @throws SimulationContextException 
	 */
	double poll() throws SimulationContextException;

	/**
	 * Does a simulation step.
	 */
	void step();

	void setContext(SimulationContext context);

	long getId();

	void setId(long id);

}
