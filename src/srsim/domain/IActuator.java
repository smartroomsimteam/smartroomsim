package srsim.domain;

import srsim.simulator.SimulationContext;
import srsim.simulator.SimulationContextException;

/**
 * Interface for simulated actuators
 * 
 * @author phil
 * 
 */
public interface IActuator {

	int ACTIVE = 1;
	int IDLE = 0;

	/**
	 * Returns the current state of the simulated actuator
	 * 
	 * @return
	 */
	int getState();

	/**
	 * Enables the simulated actuator
	 * @throws SimulationContextException 
	 */
	void enable() throws SimulationContextException;

	/**
	 * Disables the simulated actuator
	 * @throws SimulationContextException 
	 */
	void disable() throws SimulationContextException;

	/**
	 * Performs a simulation step
	 * 
	 * @throws SimulationContextException
	 */
	void step() throws SimulationContextException;

	/**
	 * Sets the simulation context for the simulated actuator
	 * 
	 * @param context
	 */
	void setContext(SimulationContext context);

	/**
	 * Returns the id of the simulated actuator
	 * 
	 * @return
	 */
	long getId();

	/**
	 * Sets the id of the simulated actuator
	 * 
	 * @param id
	 */
	void setId(long id);

	/**
	 * Returns the current energy consumtion of the simulated actuator in watts
	 * 
	 * @return
	 */
	double getEnergyConsumption();

	/**
	 * Turns up the actuator, e.g. increasing the brightness of a light
	 * @throws SimulationContextException 
	 */
	void turnUp() throws SimulationContextException;
	
	/**
	 * Turns down the actuator, e.g. decreasing the brightness of a light
	 * @throws SimulationContextException 
	 */
	void turnDown() throws SimulationContextException;

}
