# README #

This is the simulator part of the Smartroom simulator for our Ubicomp project.

### What is this repository for? ###

Use this repo to get the latest code.

### How do I get set up? ###

* Get a git client
* clone the repository (https://pbeh@bitbucket.org/smartroomsimteam/smartroomsim.git)
* unless you used your IDE for the previous steps, import the project into your IDE
* Make sure the libraries in the "lib" directory are added to the classpath. (The project is an eclipse project so if you use eclipse, importing will be all you need to do to run the code and the testcases)
* Otherwise: In order to run the tests, make sure to add junit to the classpath

### Contribution guidelines ###

* When adding new classes to any of the backend-packages, make sure they are covered by the testcases. Add a new testcase if needed.
* Same holds true for major additions to the existing classes
* when done create a branch for your modifications and submit a pull request 